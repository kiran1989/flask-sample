from flask import Flask, render_template, request

app = Flask(__name__)
@app.route('/')
def index():
	data = "kiran"
	return render_template('index.html',data=data)

@app.route('/dashboard')
def dashboard():
	return render_template('dashboard.html')


if __name__ == '__main__':
	app.run('0.0.0.0','80',debug=True)
